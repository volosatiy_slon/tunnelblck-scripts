# DNS rewrite scripts for TunnelBlick (macos) #

### Description ###
TunnelBlick software ignore NS congfguration provided in up and down scripts

### Usage ###
1. Copy scripts, provided bellow to folder where config, keys, cert is. All 
files must be in one folder
2. Edit scripts (insert IP address of name server that you use while VPN 
connection and folder to store temp file)
3. rename folder to _foldername_.**tblk**
4. import it (Drag and drop it to tunnelblick window)
    
### Scripts ###
connected.sh
```
#! /bin/bash
VPNCONFDIR=~/vpnconf
VPNNS='1.1.1.1'
DEFIFACE=$(netstat -nrf inet | grep default | sed 's/.* \([^ ]\)/\1/')
DEFIFACEALIASE=$(networksetup -listnetworkserviceorder | grep $DEFIFACE | egrep -o ": (.+)," | sed 's/^: \(.*\),$/\1/')
CURRENTNS=$(networksetup -getdnsservers $DEFIFACEALIASE | tr '\n' ' ')
echo $CURRENTNS > $VPNCONFDIR/currentns.tmp
networksetup -setdnsservers $DEFIFACEALIASE $VPNNS $CURRENTNS
```
post-disconnect.sh
```
#! /bin/bash
VPNCONFDIR=~/vpnconf
OLDNS=$(cat $VPNCONFDIR/currentns.tmp)
DEFIFACE=$(netstat -nrf inet | grep default | sed 's/.* \([^ ]\)/\1/')
DEFIFACEALIASE=$(networksetup -listnetworkserviceorder | grep $DEFIFACE | egrep -o ": (.+)," | sed 's/^: \(.*\),$/\1/')
networksetup -setdnsservers $DEFIFACEALIASE $OLDNS
```